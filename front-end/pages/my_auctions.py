import streamlit as st
from pages.interface_page import Page


# coleta dos dados necessários do sistema
def load_auctions(user_id, client):

    query = f"""
        query{{
        user(id: {user_id}){{
            
            name
            
            auctionsMade{{
                name
                id
                open
                winnerBid{{
                    createBy{{
                        name
                    }}
                    price
                }}

                bidsMade{{
                    price
                    createBy{{
                        name
                    }}
                }}

            }}
        }}
        }}
    """

    data = client.execute(query=query)

    return data['data']['user'][0]

# criando um menu
def auctions_menu(data):

    auctions_list = data['auctionsMade']


    return st.selectbox(
        'Meus Leilões',
        auctions_list,
        format_func = lambda x: x['name']
    )

# pagina de leiloes
class My_Auctions(Page):

    def __init__(self, user_id, client):
        self.user_id = user_id

        self.client = client

        self.data = load_auctions(user_id, client)

    def render(self):

        st.markdown('## Meus Leilões')

        auction = auctions_menu(self.data)

        if auction:
            st.markdown('## Nome')
            st.markdown(auction['name'])

            st.markdown('## Situação')
            if auction['open']:
                st.markdown('Aberto')
        
            else:
                st.markdown('Fechado')

            if auction['winnerBid']:

                bid = auction['winnerBid']
                st.markdown('## Lance Ganhador Até Agora')
                st.markdown('R$ {}'.format(bid['price']))

                st.markdown('## Usuário')
                st.markdown(bid['createBy']['name'])
            

            st.markdown('# Todos os Lances')

            for bid in auction['bidsMade']:
                
                st.markdown('### {} -----> R$ {}'.format(bid['createBy']['name'], bid['price']))


