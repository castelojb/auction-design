from pages.interface_page import Page
import streamlit as st
import plotly.graph_objects as go
import numpy as np


# elaboração da página e coleta dos elementos
def create_page():


	block_login = st.empty()
	block_login.header("Quem é voce?")

	block_nickname_input = st.empty()
	nickname = block_nickname_input.text_input("Usuário", "")

	block_password_input = st.empty()
	password = block_password_input.text_input("Senha", "")

	block_create_user = st.empty()
	new_user = block_create_user.button("Registrar")

	block_find_user = st.empty()
	find_user = block_find_user.button("Logar")


	return [nickname, password, new_user, find_user], [block_login, block_nickname_input, block_password_input, block_create_user, block_find_user]

# limpeza da tela
def clean_blocks(blocks):

	for block in blocks:

		block.empty()

# verificação de usuário no banco
def is_auth(nickname, password, client):

	query = f"""

    query{{
        user(name: "{nickname}", password: "{password}"){{
        id
        }}
    }}

	"""

	data = client.execute(query=query)

	if data['data']['user'] == []:
		return False, 0
	
	else:
		return True, data['data']['user'][0]['id']

# cadastro de usuario no banco
def create_user(name, password, client):

	query = f"""
			mutation{{
			
				addUser(name: "{name}", password: "{password}"){{
					id
				}}
			}}
			"""
	data = client.execute(query=query)

	try:
		return True, data['data']['addUser']['id']
	except:
		return False, -1


@st.cache(allow_output_mutation=True)
def get_continue(allow_output_mutation=True):

	return [False, -2]


# página de Log in
class LogIn(Page):

	def __init__(self, client):
		self.client = client

		self.contiune_context = get_continue()

	def render(self):

		auth, elements = create_page()
		
		nickname, password, register, find = auth
		args = self.contiune_context
		
		err = False
		if register:
			pass_, id_ = create_user(nickname, password, self.client)
			args[0] = pass_
			args[1] = id_
			if not args[0]: err=True
		
		if find:
			pass_, id_ = is_auth(nickname, password, self.client)
			args[0] = pass_
			args[1] = id_
			if not args[0]: err=True
		
		if args[0]:
			clean_blocks(elements)
			return int(args[1])
		
		elif err:
			st.info("Usuário e/ou senha invalidos")

		
