import streamlit as st
from pages.interface_page import Page


# carregando dados
def load_auctions(user_id, client):

    query = f"""
        query{{
        user(id: {user_id}){{
            
            name
            
            auctionsMade{{
                name
                id
                open
                winnerBid{{
                    createBy{{
                        name
                    }}
                    price
                }}

            }}
        }}
        }}
    """

    data = client.execute(query=query)

    return data['data']['user'][0]

# editando leilao
def update_auction(name, open_, auction_id, client):

    query = f"""
    mutation{{
  
        updateAuction(id: {auction_id}, name:"{name}", open: {open_}){{

            name
            open
            
        }}
    }}
    """

   
    data = client.execute(query=query)

    return data['data']['updateAuction']

# criando menu
def auctions_menu(data):

    auctions_list = data['auctionsMade']


    return st.selectbox(
        'Meus Leilões',
        auctions_list,
        format_func = lambda x: x['name']
    )

# pagina de atualização
class Update_Auctions(Page):

    def __init__(self, user_id, client):
        self.user_id = user_id

        self.client = client

        self.data = load_auctions(user_id, client)

    def render(self):

        st.markdown('## Meus Leilões')

        auction = auctions_menu(self.data)

        if auction:
            st.markdown('## Nome')
            st.markdown(auction['name'])

            st.markdown('## Situação')
            if auction['open']:
                st.markdown('Aberto')
        
            else:
                st.markdown('Fechado')

            if auction['winnerBid']:

                bid = auction['winnerBid']
                st.markdown('## Lance Ganhador Até Agora')
                st.markdown('R$ {}'.format(bid['price']))

                st.markdown('## Usuário')
                st.markdown(bid['createBy']['name'])
            

            edit = st.checkbox('Gostaria de editar?')

            if edit:

                name = st.text_input('{}'.format(auction['name']), auction['name'])

                open_ = st.checkbox('Aberto', value=auction['open'])

                if open_:
                    open_ = 'true'
                else:
                    open_ = 'false'
                if st.button('Modificar'):

                    auction_up = update_auction(name, open_, int(auction['id']), self.client)

                    st.markdown('Modificado com sucesso!')

                    st.markdown('## Nome')
                    st.markdown(auction_up['name'])

                    st.markdown('## Situação')
                    if auction_up['open']:
                        st.markdown('Aberto')
                
                    else:
                        st.markdown('Fechado')







