import streamlit as st
from pages.interface_page import Page


# coleta dos dados de leilões
def load_auctions(user_id, client):

    query = """
        query{
            auction(open:true){
                id
                name
                
                winnerBid{
                    price
                    createBy{
                        name
                    }
                }
            }
        }
    """

    data = client.execute(query=query)

    return data['data']['auction']

# montando menu
def auctions_menu(data):

    return st.selectbox(
        'Leilões Disponiveis',
        data,
        format_func = lambda x: x['name']
    )

# adicionando lance
def add_bid(price, user_id, auction_id, client):

    query = f"""
    mutation{{
        addBid(price: {price}, createBy: {user_id}, toAuction: {auction_id}){{
            id
        }}
    }}
    """

    data = client.execute(query=query)
    
    return data

# pagina de lances
class New_Bid(Page):

    def __init__(self, user_id, client):
        self.user_id = user_id

        self.client = client

        self.data = load_auctions(user_id, client)

    def render(self):

        st.markdown('## Salão dos Leilões')

        auction = auctions_menu(self.data)

        if auction:
            st.markdown('## Nome')
            st.markdown(auction['name'])

            if auction['winnerBid']:

                bid = auction['winnerBid']
                st.markdown('## Lance Ganhador Até Agora')
                st.markdown('R$ {}'.format(bid['price']))

                st.markdown('## Usuário')
                st.markdown(bid['createBy']['name'])

            else:
                st.markdown("Sem lances ainda")        

            st.markdown('## Novo Lance')

            price = st.number_input('Faça sua proposta')
            
            if st.button('Enviar'):
                bid = add_bid(price, self.user_id, int(auction['id']), self.client)

                
                st.markdown('Proposta enviada com sucesso!')




