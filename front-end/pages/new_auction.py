import streamlit as st
from pages.interface_page import Page


# coleta de informações do sistema
def create_auction(name, user_id, client):

    query = f"""
    mutation{{
        addAuction(name: "{name}", createBy: {user_id}){{
          id
          name  
        }}
    }}
    """

    data = client.execute(query=query)


# pagina de leiloes
class New_Auction(Page):

    def __init__(self, user_id, client):
        self.user_id = user_id

        self.client = client

    def render(self):

        st.markdown('## Novo Leilão')

        name = st.text_input('Nome do seu leilão')

        build = st.button('Criar')

        if build and name:

            create_auction(name, self.user_id, self.client)

            st.markdown("Leilão Criado com Sucesso!")
