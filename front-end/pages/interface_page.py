import abc

"""
Mandatory functions for a page
"""


class Page(abc.ABC):

	@abc.abstractmethod
	def render(self):
		"""
		Function to render a page

		Returns:
			Instantiating the desired elements

		"""
		pass

