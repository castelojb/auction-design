import streamlit as st
from pages.interface_page import Page


# coleta das informações necessarias da pagina
def load_auctions(user_id, client):

    query = f"""
        query{{
            user(id: {user_id}){{
                bidsMade{{
                    price
                    toAuction{{
                        name
                        winnerBid{{
                            price
                            createBy{{
                                name
                            }}
                        }}
                        
                    }}
                }}
            }}
        }}
    """

    data = client.execute(query=query)

    return data['data']['user'][0]['bidsMade']

# criando menu
def auctions_menu(data):


    return st.selectbox(
        'Leilões que eu participei',
        data,
        format_func = lambda x: x['toAuction']['name']
    )

# Pagina de lances
class My_Bids(Page):

    def __init__(self, user_id, client):
        self.user_id = user_id

        self.client = client

        self.data = load_auctions(user_id, client)

    def render(self):

        st.markdown('## Todos os meus lances')

        for bid in self.data:
            
            st.markdown('### {} -----> R$ {}'.format(bid['toAuction']['name'], bid['price']))
