# Interface: Leilão

## Escopo
Este projeto foi dividido em 2 camadas: interface e api-rest. Nesta camada, foi desenvolvida uma interface para interagir com a camada de api.

## Regras
O leilão apresenta regras de funcionamento que foram refletidas no desenvolvimento

### 1. Usuário
 * O usuário pode criar e editar seus leilões
 * O usuŕio pode dar lances em todos os leilões disponiveis
 * O usuário não pode editar lances

### 2. Leilão
 * Pode ser editado
 * Pode ser fechado pelo usuario
 * Deve escolher o melhor lance

### 3. Lance
 * Pode ser qualquer valor
 * Não pode ser editado


## Módulos e Ferramentas
Esta api foi desenvolvida utilizando o Streamlit, agilizando o processo de elaboração das telas

### Módulos
Neste momento, será feita uma explicação de cada módulo deste projeto

#### 1. main.py
Arquivo principal do projeto, todas as páginas são criadas e a plicação é iniciada

#### 2. about.py
Página de explicação do projeto, deveria ser...

#### 3. log_in
Neste pacote, é feita uma verificação se o usuário esta ou não no sistema, com possibiblidade de cadastro

#### 4. my_auctions.py
Basicamente, um relatorio de todos os leilões feitos pelo usuário

#### 5. my_bids.py
Todos os lances realizados pelo usuário no sistema

#### 6. new_auction.py
Novo leilão pelo usuário

#### 7. new_bid.py
Novo lance em um leilão disponivel

### Comandos
Aconselho, fortemente, utilizar o gestor de dependências Anaconda ou
Miniconda para gerenciar suas dependências,
tendo um deles em sua máquina, siga esses passos:

Abra o terminal no diretorio linguagem-script
use o comando $ conda env create -f environment.yml
Veja se o ambiente foi instalado corretamente com $ conda env list
Ative o ambiente com $ conda activate l_script


Em caso de duvidas, ver tutorial


Com isso, voce possui todas as dependencias utilizadas nesse projeto, para executar o aplicativo, use o comando
$ streamlit run main.py --server.port 8080 --server.baseUrlPath "/home/" --server.enableCORS false