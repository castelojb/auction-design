import streamlit as st
from streamlit.hashing import _CodeHasher


from pages.about import About
from pages.log_in import LogIn
from pages.my_auctions import My_Auctions
from pages.new_auction import New_Auction
from pages.update_auction import Update_Auctions
from pages.new_bid import New_Bid
from pages.my_bids import My_Bids

from python_graphql_client import GraphqlClient

# conexão com o cliente
client = GraphqlClient(endpoint="http://localhost:4000")

# carregando as paginas
def load_pages(id_, client):
	"""
	Loading pages to be loaded into the application

	Returns:
		dict: {page name: Page}
	"""

	pages = dict()

	pages['Meus Leilões'] = My_Auctions(id_, client)

	pages['Novo Leilão'] = New_Auction(id_, client)

	pages['Modificar Leilão'] = Update_Auctions(id_, client)

	pages['Novo Lance'] = New_Bid(id_, client)

	pages['Todos os meus lances'] = My_Bids(id_, client)
	
	pages['Sobre'] = About()
	
	return pages

# aplicação principal
def main(id_, client):
	"""
	Application body and route configuration
	"""


	pages = load_pages(id_, client)

	st.header('O Melhor Leilão de TODOS OS TEMPOS, completamente legalizado')

	st.sidebar.markdown("# Navegação")

	goto = st.sidebar.radio("Ir para", list(pages.keys()))

	pages[goto].render()



# iniciando o servidor
if __name__ == "__main__":
	
	log_in = LogIn(client)

	user_id = log_in.render()
	
	if user_id:
		main(user_id, client)
