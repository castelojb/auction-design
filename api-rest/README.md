# API: Leilão

## Escopo
Este projeto foi dividido em 2 camadas: interface e api-rest. Nesta camada, foi desenvolvida uma api completa para gerenciar um leilão.

## Regras
O leilão apresenta regras de funcionamento que foram refletidas no desenvolvimento

### 1. Usuário
 * O usuário pode criar e editar seus leilões
 * O usuŕio pode dar lances em todos os leilões disponiveis
 * O usuário não pode editar lances

### 2. Leilão
 * Pode ser editado
 * Pode ser fechado pelo usuario
 * Deve escolher o melhor lance

### 3. Lance
 * Pode ser qualquer valor
 * Não pode ser editado


## Módulos e Ferramentas
Esta api foi desenvolvida utilizando o GraphQL, ou seja, podemos obter, do lado do cliente, qualquer dado em uma estrutura desejada quando necessario, reduzindo o numero de requisições de uma api rest tradicional. Além disso, foi utilizado o Prisma para gerenciar as operações com bancos de dados, facilitando consultas, mutações, criação de tabelas e, se necessário, migrar para outro banco relacional ou MongoDB.

### Módulos
Neste momento, será feita uma explicação de cada módulo deste projeto

#### 1. index.js
Arquivo principal do projeto, todas as consultas são organizadas e o servidor é executado

#### 2. schema.graphql
Para determinar as relações entre entidades na api, é criado um arquivo contendo as funções disponiveis para o usuário, entidades e seus atributos, e mutações (adição, remoção, edição) disponiveis. Resumindo, esse é um contrato que garante o usuário dos retornos que a api dará

#### 3. Resolvers
Neste pacote, são implementados todas as consultas que foram elencadas no schema.graphql, incluindo acesso a entidades e suas relações.

#### Pasta Prisma
Nesta pasta, está a criação e gerenciamento do banco de dados, "schema.prisma" determina as entidades e suas relações com as outras, além de seus atributos, essa é uma abstração do prisma para criação das tabelas no banco de dados, toda migração pode ser versionada e, dentro dela, está um readme.md com a criação explicita de cada banco

### Comandos
Um guia de como operar a api
#### Prisma
Para criar uma migração, use
* yarn prisma migrate save --experimental

Para executar uma migração use:
* yarn prisma migrate up --experimental

Finalmente, para montar o cliente, use:
* yarn prisma generate

Pronto! Sua aplicação ja pode usar o prisma

#### GraphQL

Para criar o servidor, basta executar o codigo em node:
* node src/index.js

O servidor será alocado no endereço:
* http://localhost:4000

O cliente poderá se conectar por esse link, para acessar a interface da api, basta usar esse link no navegador.
