// modulo para implementar as relações de Bid com as outras entidades

// acesso de usuário
function createBy(parent, args, context){

    return context.prisma.bid.findOne({
        where: {id: parent.id}
    }).createBy()    
    
}

// acesso de leilão
function toAuction(parent, args, context){

    return context.prisma.bid.findOne({
        where: {id: parent.id}
    }).toAuction()    
    
}

module.exports = {
    createBy,
    toAuction
}
