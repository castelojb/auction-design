// modulo para implementar todas as relações de usuários com as outras entidades

// função auxiliar
function query_wrapper(args){
    var data_update = {}

    for (const [key, value] of Object.entries(args)) {
        data_update[key] = value
    }

    return data_update
}

// acesso de lances
function bidsMade(parent, args, context) {
    
    const query = query_wrapper(args)

    return  context.prisma.user.findOne({ where: {id: parent.id} }).bidsMade({where: query})


}

// acesso de leiloes
function auctionsMade(parent, args, context) {

    const query = query_wrapper(args)

    return  context.prisma.user.findOne({ where: {id: parent.id} }).auctionsMade({where: query})

}

module.exports = {
    bidsMade,
    auctionsMade
}