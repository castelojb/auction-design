// Módulo para implementar as relações de Auctions com as outras entidades

// função auxiliar
function query_wrapper(args){
    var data_update = {}

    for (const [key, value] of Object.entries(args)) {
        data_update[key] = value
    }

    return data_update
}

// Função para obter o usuário desse atributo
function createBy(parent, args, context){

    return context.prisma.auction.findOne({
        where: {id: parent.id}
    }).createBy()
}

// função para obter o lance desse atributo
function winnerBid(parent, args, context){

    
    return context.prisma.auction.findOne({
        where: {id: parent.id}
    }).winnerBid()
}

// função para obter o lance desse atributo
function bidsMade(parent, args, context){

    const query = query_wrapper(args)

    return context.prisma.auction.findOne({
        where: {id: parent.id}
    }).bidsMade({
        where: query
    })
}

module.exports = {
    createBy,
    winnerBid,
    bidsMade
}
