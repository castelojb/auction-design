// Neste pacote foram criadas todas as consultas disponiveis na Query do schema.graphql

// Função auxiliar
function query_wrapper(args){
    var querys = []
    
    for (const [key, value] of Object.entries(args)) {

        var data = {}

        data[key] = value

        querys.push(data)
    }

    return querys
}

// consulta na base de clientes
async function user(parent, args, context, info){

    const query = query_wrapper(args)

    const where = {

        AND: query
    }

    const users = await context.prisma.user.findMany({
        where
    })

    return users
}

// consulta na base de leilões
async function auction(parent, args, context, info){

    const query = query_wrapper(args)

    const where = {

        AND: query
    }

    const auctions = await context.prisma.auction.findMany({
        where
    })

    return auctions
}

// consulta na base de lances
async function bid(parent, args, context, info){
    
    const query = query_wrapper(args)

    const where = {

        AND: query
    }

    const bids = await context.prisma.bid.findMany({
        where
    })

    return bids
}

module.exports = {
    user,
    auction,
    bid
}
