// neste módulo foi implementado todas as mutações do schema.graphql

// função auxiliar
function update_wrapper(args){
    var data_update = {}

    for (const [key, value] of Object.entries(args)) {
        data_update[key] = value
    }

    return data_update
}

// adição de usuário
function addUser(parent, args, context, info){

    const new_data = update_wrapper(args)

    const new_user = context.prisma.user.create({
        data: new_data
    })

    return new_user

}

// atualização de usuário
function updateUser(parent, args, context, info){
    

    var data_update = update_wrapper(args)

    delete data_update.id

    const up_user = context.prisma.user.update({
        where: {id: args.id},

        data: data_update
    })

    return up_user
}

// remoção do usuário
function deleteUser(parent, args, context, info){
    
    const del_user = context.prisma.user.delete({
        where: {id: args.id}
    })

    return del_user
}

// adição de leilão
function addAuction(parent, args, context, info){

    
    const auction = context.prisma.auction.create({
        data:{
            name: args.name,
            createBy: {connect: {id: args.createBy}}
        }
    })

    return auction

}

// edição de leilão
function updateAuction(parent, args, context, info){

    var new_data = update_wrapper(args)

    delete new_data.id

    if(args.winnerBid){

        new_data[winnerBid] = {
            connect: {where: {id: args.winnerBid}}
        }
    }

    var auction = context.prisma.auction.update({
        where: {id: args.id},

        data: new_data
    })
    
    return auction
}

// remoção de leilão
function deleteAuction(parent, args, context, info){
    const auction = context.prisma.auction.delete({
        where: {id: args.id}
    })

    return auction
}

// adição de lance, toda adição resulta em uma pesquisa pelo maior lance de um leilão e sua edição
async function addBid(parent, args, context, info){
    
    const bid = await context.prisma.bid.create({
        data:{
            createBy: {connect: {id: args.createBy}},
            toAuction: {connect: {id: args.toAuction}},
            price: args.price
        }
    })


    var max_bid = await context.prisma.auction.findOne({
        where: {id: args.toAuction}
    }).bidsMade({
        distinct: "price",
        orderBy: {
            price: "desc"
        },
        take: 1
    })

    await context.prisma.auction.update({
        where: {id: args.toAuction},

        data:{
            winnerBid: {connect: {id: max_bid[0].id}}
        }
    })
    
    return bid
}

// toda remoção de lance resulta em uma busca pelo maior lance para edição do leilao
async function deleteBid(parent, args, context, info){

    const auction = await context.prisma.bid.findOne({
        where: {id: args.id}
    }).toAuction()

    const bid = await context.prisma.bid.delete({
        where: {id: args.id}
    })

    var max_bid = await context.prisma.auction.findOne({
        where: {id: auction.id}
    }).bidsMade({
        distinct: "price",
        orderBy: {
            price: "desc"
        },
        take: 1
    })

    await context.prisma.auction.update({
        where: {id: auction.id},

        data:{
            winnerBid: {connect: {id: max_bid[0].id}}
        }
    })

    return bid
}

module.exports = {
    addUser,
    updateUser,
    deleteUser,

    addAuction,
    updateAuction,
    deleteAuction,

    addBid,
    deleteBid
}