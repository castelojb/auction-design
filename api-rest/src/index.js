// bibliotecas usadas

const { GraphQLServer } = require('graphql-yoga')
const { PrismaClient } = require('@prisma/client')
const Query = require('./resolvers/Query')
const Mutation = require('./resolvers/Mutation')
const User = require('./resolvers/User')
const Auction = require('./resolvers/Auction')
const Bid = require('./resolvers/Bid')

// uso do cliente prisma
const prisma = new PrismaClient({
  errorFormat: 'minimal'
})

// consultas implementadas
const resolvers = {
  Query,
  Mutation,
  User,
  Auction,
  Bid
}

// criando o servidor
const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  
  resolvers,
  
  context: {
    prisma,
  }
})

// iniciando serviço
server.start(() => console.log(`Server is running on http://localhost:4000`))